import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import axios from 'axios'
import VueAxios from "vue-axios";
import GAuth from 'vue-google-oauth2'

Vue.config.productionTip = false;
Vue.use(VueAxios, axios);
const gauthOption = {
  clientId: 'CLIENT_ID.apps.googleusercontent.com',
  scope: 'profile email',
  prompt: 'select_account'
};
Vue.use(GAuth, gauthOption);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
