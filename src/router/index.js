import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/constructor',
    component: () => import('./../views/Constructor.vue'),
  },
  {
    path: '/',
    component: () => import('./../views/Report_list.vue'),
  },
  {
    path: '/report',
    component: () => import('./../views/Report.vue'),
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
